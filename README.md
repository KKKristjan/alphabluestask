# AlphaBlues task #
Personal.json is where the data is stored.
In file task.py is the solution to the task and in test.py are the tests. The requests in python language for testing where generated with Postman.

## How to run solution and test ##
1. Start the localhost server with command "python task.py"
2. Run command "python test.py"
