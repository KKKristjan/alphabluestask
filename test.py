import requests
print("Adding a user:")
url = "http://127.0.0.1:81/user/add"

payload = "{\n\t\"user_name\" : \"Demo User\",\n\t\"password\" : \"securePassword\"\n}"
headers = {
    'content-type': "application/json",
    'cache-control': "no-cache",
    'postman-token': "61ee4ddf-78d6-67c4-88e7-4ed8fb80d40c"
    }

response = requests.request("POST", url, data=payload, headers=headers)

print(response.text)

print("Changing a password:")
url = "http://127.0.0.1:81/user/update"

payload = "{\n\t\"user_name\" : \"Demo User\",\n\t\"password\" : \"securePassword\",\n\t\"newpassword\": \"ExtraSecurePassword\"\n}"
headers = {
    'content-type': "application/json",
    'cache-control': "no-cache",
    'postman-token': "719ada70-71ac-7880-a844-0e250f4c1c3b"
    }

response = requests.request("POST", url, data=payload, headers=headers)

print(response.text)

print("Logging in succesfully: ")
url = "http://127.0.0.1:81/login"

payload = "{\n\t\"user_name\" : \"Demo User\",\n\t\"password\" : \"ExtraSecurePassword\"\n}"
headers = {
    'content-type': "application/json",
    'cache-control': "no-cache",
    'postman-token': "22c744b7-d4c0-15b8-d538-91118110d675"
    }

response = requests.request("POST", url, data=payload, headers=headers)

print(response.text)
print("Logging in where user's credentials have expired: ")

url = "http://127.0.0.1:81/login"

payload = "{\n\t\"user_name\" : \"newUser\",\n\t\"password\" : \"password\"\n}"
headers = {
    'content-type': "application/json",
    'cache-control': "no-cache",
    'postman-token': "2dca04be-c81c-1523-978a-f6904508ef80"
    }

response = requests.request("POST", url, data=payload, headers=headers)

print(response.text)
print("Logging in where user's credentials are invalid: ")

url = "http://127.0.0.1:81/login"

payload = "{\n\t\"user_name\" : \"newUser\",\n\t\"password\" : \"wrongpassword\"\n}"
headers = {
    'content-type': "application/json",
    'cache-control': "no-cache",
    'postman-token': "2dca04be-c81c-1523-978a-f6904508ef80"
    }

response = requests.request("POST", url, data=payload, headers=headers)

print(response.text)
