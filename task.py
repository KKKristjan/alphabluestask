from flask import Flask, request
from flask_restful import Api, Resource, reqparse
from datetime import datetime as dt
from werkzeug.security import generate_password_hash, check_password_hash

import datetime
import json

app = Flask(__name__)
api = Api(app)

@app.route('/user/add', methods=['POST'])
def addUser():
    req_data = request.get_json()
    user_name = req_data['user_name']
    result = list(filter(lambda x : user_name in x["user_name"], db["users"]))
    if (len(result) > 0):
        return "There already is such a user with this username", 400
    else:
        dateString = addSeconds()
        password = generate_password_hash(req_data['password'], "sha256")
        user = {
            "user_name": user_name,
            "password": password,
            "expires": dateString
        }
        db["users"].append(user)
        writeJson('personal.json')
        return 'Added user {}'.format(user_name), 201

@app.route('/user/update', methods=['POST'])
def updatePassword():
    req_data = request.get_json()
    user_name = req_data['user_name']
    password = req_data['password']
    newpassword = req_data['newpassword']
    for element in db["users"]:
        if element["user_name"] == user_name and check_password_hash(element["password"], password):
            element["password"] = generate_password_hash(req_data['newpassword'],"sha256")
            element["expires"] = addSeconds()
            writeJson('personal.json')
            return 'Updated password for {}'.format(user_name)
    return 'username or password is invalid', 401

@app.route('/login', methods=['POST'])
def userLogin():
    req_data = request.get_json()
    user_name = req_data['user_name']
    password = req_data['password']
    for element in db["users"]:
        if element["user_name"] == user_name and check_password_hash(element["password"], password) and not(isExpired(element["expires"])):
            return "User {} logged in".format(user_name)
        if element["user_name"] == user_name and check_password_hash(element["password"], password) and isExpired(element["expires"]):
            return "Please provide new password"
    return 'username or password is invalid', 401

def writeJson(filename):
    with open(filename, 'w') as json_file:
        json.dump(db, json_file)

def readJson(filename):
    with open(filename, 'r') as json_file:
        data = json_file.read()
    return json.loads(data)

def isExpired(date):
    now = dt.now()
    date_time_obj = datetime.datetime.strptime(date, '%d/%m/%Y %H:%M:%S')
    if now > date_time_obj:
        return True
    else:
        return False

def addSeconds():
    now = dt.now()
    b = now + datetime.timedelta(0, 30)
    return b.strftime("%d/%m/%Y %H:%M:%S")

db = readJson('personal.json')
app.run(host='0.0.0.0', port= 81)
